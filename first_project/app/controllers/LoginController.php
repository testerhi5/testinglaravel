<?php
class LoginController extends BaseController {

	

	public function showLogin()
	{
		$data = array();
		 if (Auth::check()) {
			$data = Auth::user();
		}
		
		return View::make('user/login', array('data'=>$data));
	}
	public function postLogin()
	{

		if(Input::get('signup')){
		
		
			$name = Input::get('name');
			$password = Input::get('password');
			$username = Input::get('username');
			$email = Input::get('email');
			$rules = array(
				'email'    => 'required|email', // make sure the email is an actual email
				'username' => 'required',  
				'name' 	   => 'required',  
				'password' => 'required|alphaNum|min:8', // password can only be alphanumeric and has to be greater than 3 characters
				'password_confirm' => 'required|same:password' 
			);
			
			$validator = Validator::make(Input::all(), $rules);
			
			if ($validator->fails()) {
				return Redirect::to('login')
					->withErrors($validator) // send back all errors to the login form
					->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
			} else {
				$user = new User;
				$user->name = $name;
				$user->photo = 'photo';
				$user->email = $email;
				$user->password =Hash::make( $password );
				$user->save();
				
				$profile = new Profile();
				$profile->username = $username;
				$profile->uid = '0';
				$profile->access_token = '0';
				$profile->access_token_secret = '0';
				$profile = $user->profiles()->save($profile);
				$profile->save();
				$user = $profile->user;
				Auth::login($user);
				return Redirect::to('/')->with('message', 'Logged in with Facebook');
			}
		}elseif(Input::get('signin')){
			
			$rules = array(
				'email'    => 'required|email', // make sure the email is an actual email
				'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
			);

			// run the validation rules on the inputs from the form
			$validator = Validator::make(Input::all(), $rules);

			// if the validator fails, redirect back to the form
			if ($validator->fails()) {
				return Redirect::to('login')
					->withErrors($validator) // send back all errors to the login form
					->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
			} else {

				// create our user data for the authentication
				$userdata = array(
					'email' 	=> Input::get('email'),
					'password' 	=> Input::get('password')
				);
				
				// attempt to do the login
				if (Auth::attempt($userdata)) {
						

			 
					// validation successful!
					// redirect them to the secure section or whatever
					// return Redirect::to('secure');
					// for now we'll just echo success (even though echoing in a controller is bad)
					return Redirect::to('login')->with('message', 'Logged in with Simple User');

				} else {	 	
				 
					// validation not successful, send back to form	
					return Redirect::to('/')->with('message', 'Wrong Email or Password');

				}

			}
		}
	}
	 
}
