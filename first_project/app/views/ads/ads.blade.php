@extends('layouts.default')
@section('content')
	
	@if(Session::has('message'))
		{{ Session::get('message')}}
	@endif
	<a href="ads/new">Add New</a>
	<a href="accountinfo">Account Info</a>
	<table align="center" class="table" border="1">
		<tr>
			<td>Sr No.</td>
			<td>Image</td>
			<td>Ads Surface</td>
			<td>Ads type</td>
			<td>Ads Price</td>
			<td>Property type</td>
			<td colspan="2">Action</td>
		</tr>
		
	@if (!empty($ads))
		@foreach ($ads as $ad)
		<tr>
			<td>{{$ad->id}}</td>
			<td><img src="<?php echo URL::to('/uploads/thumb/'.$ad->picture)?>"/></td>
			<td>{{$ad->surface}}</td>
			<td>{{$ad->type}}</td>
			<td>{{$ad->price}}</td>
			<td>{{$ad->property_type}}</td>
			<td><a href="{{URL::to('/updateform/'.$ad->id)}}">Edit</a></td>
			<td><a href="{{URL::to('/deletead/'.$ad->id)}}" onclick="if(!confirm('Are you sure to delete this item?')){return false;};" >Delete</a></td>
		</tr>
		@endforeach
	@else

	 <tr>
		<td>No data Found</td>
	 </tr>
	@endif
	</table>
@stop