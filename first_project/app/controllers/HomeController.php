<?php

class HomeController extends BaseController
 {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		if (!Auth::check()) {
			return Redirect::to('/login')->with('message', 'Please Login First');
		}
		$id1 = Auth::user()->id;
		$ads = Ads::where('user_id', '=', $id1)->get();
		return View::make('ads/ads',array('ads'=>$ads));
	}
	public function adsform()
	{
			if (!Auth::check())
			{
			return Redirect::to('/login')->with('message', 'Please Login First');
			}
			$id = Auth::user()->id;
			return View::make('ads/form',array('user_id'=>$id));
	}
	
	public function showInfo()
	{
		if (!Auth::check())
		{
			return Redirect::to('/login')->with('message', 'Please Login First');
		}
		else
		{
			$id1 = Auth::user()->id;
			$user = Users::find($id1);
			return View::make('ads/accountinfo',array('user'=>$user));
		
		}
	
	}
	
	
		public function postads()
	{	
	
		if(Input::get('adsbutton'))
		{
			
			 $picture = Input::file('picture');
			// print_r($picture);
			$user_id = Input::get('user_id');
			$surface = Input::get('surface');
			$price = Input::get('price');
			$address = Input::get('address');
			$description = Input::get('description');
			$type = Input::get('type');
			$property_type = Input::get('property_type');
			$isactive = Input::get('isactive');

			$rules = array(
				'picture'    => 'required',
				'surface' 	   => 'required',  
				'price'    => 'required',
				'address' => 'required',  
				'description' 	   => 'required'
				
			);
			$validator = Validator::make(Input::all(), $rules);
					
					
			if ($validator->fails())
			{
				return Redirect::to('ads/new')
					->withErrors($validator); // send back all errors to the login form
			}

			else
			{
				
				$ads = new Ads;
				if (Input::hasFile('picture')) 
				{
				 $path   = public_path() . '/uploads/';
					$thumb   = public_path() . '/uploads/thumb/';
					$avatar = md5(time()) . '.' . Str::lower(Input::file('picture')->getClientOriginalExtension());
					Image::make(Input::file('picture'))->fit(80)->save($thumb . $avatar);
					Input::file('picture')->move($path , $avatar);
					$ads->picture=$avatar;
				}
				$ads->user_id = $user_id;
				$ads->surface = $surface;
				$ads->price = $price;
				$ads->address = $address;
				$ads->description = $description;
				$ads->type = $type;
				$ads->property_type = $property_type;
				$ads->isactive = $isactive;
				$ads->save();
				if($ads)
				{
					return Redirect::to('/')->with('message', 'Added Successfully');
				}
				else
				{
					return Redirect::to('/')->with('message', 'Unable to add');
				
				}		
			}
		}
	}	
	
	public function updateads($id='')
	{
	 if(empty($id))
		return Redirect::to('/')->with('message', 'Please select Id');;
		
		//$ads = Ads::where('id', '=', $id)->get();
		$ads = Ads::find($id); $id = Auth::user()->id;
		return View::make('ads/updateform',array('ad'=>$ads,'user_id'=>$id));

	}
	
	public function postadupdate()
	{
		
			if(Input::get('updatebtn'))
		{ 
			$surface = Input::get('surface');
			$id = Input::get('id');
			$price = Input::get('price');
			$address = Input::get('address');
			$description = Input::get('description');
			$type = Input::get('type');
			$property_type = Input::get('property_type');
			$isactive = Input::get('isactive');
			$rules = array(
				
				'surface' 	   => 'required',  
				'price'    => 'required',
				'address' => 'required',  
				'description' 	 => 'required'
				
			);
			$validator = Validator::make(Input::all(), $rules);
					
					
			if ($validator->fails())
			{
				return Redirect::to('updateform/'.$id)
					->withErrors($validator); // send back all errors to the login form
					die;
			}

		
				$ads = new Ads;
				
				if (Input::hasFile('picture')) 
				{
					$path   = public_path() . '/uploads/';
					$thumb   = public_path() . '/uploads/thumb/';
					$avatar = md5(time()) . '.' . Str::lower(Input::file('picture')->getClientOriginalExtension());
					$picture = Input::file('picture');	
					Image::make($picture)->fit(80)->save($thumb . $avatar);
					$photo=Input::file('picture')->move($path,$avatar);
					Ads::where('id', $id)->update(array('picture'=> $avatar));
				}
				
				 Ads::where('id', $id)->update(array(
				 
				'surface'    =>  $surface,
				'price' =>  $price,
				'address'  => $address,
				'description' => $description,
				'type' => $type,
				'property_type' => $property_type,
				'isactive' => $isactive
				
				));
				return Redirect::to('/')->with('message', 'Updated Successfully');
			}
	}
	
	public function deletead($id)
	{
		DB::table('ads')->delete($id);
		return Redirect::to('/')->with('message', 'Deleted Successfully');
	}
	
	

}		