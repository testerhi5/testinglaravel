@extends('layouts.default')
@section('content')
	
	@if(Session::has('message'))
		{{ Session::get('message')}}
	@endif
	
	
			{{ Form::open(array('url' => 'form','method'=>'post','files'=>true)) }}
				<p>
					<!--@if ($errors->has())
					
						@foreach ($errors->all() as $error)
						<span class="alert-error clearfix">	{{ $error }}	</span>
						@endforeach
					
					@endif-->
				</p>
				<table  class="table" border="1">
					<tr>
						<th colspan="2">Add Ads</th>
					</tr>
					
					<tr>
						<td>{{ Form::hidden('user_id',$user_id) }}</td>
					</tr>
					
					
					<tr>
						<td>{{ Form::label('picture', 'Image') }}</td>
						<td>{{ Form::file('picture') }}
						<br>{{ $errors->first('picture') }}
						</td>
					</tr>
					
					<tr>
					<td>{{ Form::label('surface', 'Surface') }}</td>
						<td>{{ Form::text('surface', Input::old('surface'), array('placeholder' => 'Surface')) }} 
						<br>{{ $errors->first('surface') }}
						</td>
					</tr>
					
					<tr>
					<td>{{ Form::label('price', 'Price') }}</td>
						<td>{{ Form::text('price', Input::old('price'), array('placeholder' => 'Price')) }} 
						<br>{{ $errors->first('price') }}
						</td>
					</tr>
					
					
					<tr>
					<td>{{ Form::label('address', 'Address') }}</td>
					
						<td>{{ Form::text('address', Input::old('address'), array('placeholder' => 'Address')) }} 
						<br>{{ $errors->first('address') }}
						</td>
					</tr>
					
					<tr>
					<td>{{ Form::label('description', 'Description') }}</td>
					
						<td>{{ Form::text('description', Input::old('description'), array('placeholder' => 'Description')) }} 
						<br>{{ $errors->first('description') }}
						</td>
					</tr>
					
						<tr>
						<td>{{ Form::label('type', 'Type') }}</td>
						<td>{{ 'Rent' }} {{Form::radio('type', 'rent', true)}} {{ 'Buy' }} {{Form::radio('type', 'buy', true)}}</td>
						<br>{{ $errors->first('type') }}
						
					</tr>
					
					
					<tr>
						<td>{{ Form::label('property_type', 'Property_type') }}</td>
						<td>{{ 'Appartment' }} {{Form::radio('property_type', 'appartment', true)}} {{ 'Villa' }} {{Form::radio('property_type', 'villa', true)}}</td>
						
						<br>{{ $errors->first('property_type') }}
					
					</tr>
					
					<tr>
						<td>{{ Form::label('isactive', 'Isactive') }}</td>
						<td>{{ 'Yes' }} {{Form::radio('isactive', 'y', true)}} {{ 'No' }} {{Form::radio('isactive', 'n', true)}}</td>
						<br>{{ $errors->first('isactive') }}
						
					</tr>
					<tr><td colspan="2" style="text-align:center;">{{ Form::submit('ADD Ad',array('name'=>'adsbutton','class'=>"btn btn-info")) }}</td></tr>
					
				</table>
			{{ Form::close() }}
			 
	 
@stop