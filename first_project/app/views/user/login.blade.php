@extends('layouts.default')
@section('content')
	
	@if(Session::has('message'))
		{{ Session::get('message')}}
	@endif
	
	@if (empty($data))

	<table align="center" class="table">
		<tr>
			<td valign="top">
			{{ Form::open(array('url' => 'login','method'=>'post')) }}
				<table  class="table">
					<tr>
						<th colspan="2">Login</th>
					</tr>
					<tr>
						<td>{{ Form::label('email', 'Email Address') }}</td>
						<td>{{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com')) }}
						<br>{{ $errors->first('email') }}
						</td>
					</tr>
					<tr>
						<td>{{ Form::label('password', 'Password') }}</td>
						<td>{{ Form::password('password',array('placeholder' => 'Password')) }}
						<br>{{ $errors->first('password') }}
						</td>
					</tr>
					<tr>
						<td><input type="submit" class="btn btn-success" name="signin" value="login"></td>
					</tr>
				</table>
			{{ Form::close() }}
			</td>
			<td>
			{{ Form::open(array('url' => 'login','method'=>'post')) }}
				<p>
					<!--@if ($errors->has())
					
						@foreach ($errors->all() as $error)
						<span class="alert-error clearfix">	{{ $error }}	</span>
						@endforeach
					
					@endif-->
				</p>
				<table  class="table">
					<tr>
						<th colspan="2">Registartion</th>
					</tr>
					<tr>
						<td>{{ Form::label('name', 'Name') }}</td>
						<td>{{ Form::text('name', Input::old('name'), array('placeholder' => 'Name')) }} 
						<br>{{ $errors->first('name') }}
						</td>
						
					</tr>
					<tr>
						<td>{{ Form::label('email', 'Email Address') }}</td>
						<td>{{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com')) }}
						<br>{{ $errors->first('email') }}
						</td>
					</tr>
					<tr>
						<td>{{ Form::label('username', 'Username') }}</td>
						<td>{{ Form::text('username', Input::old('name'), array('placeholder' => 'User Name')) }} 
						<br>{{ $errors->first('username') }}
						</td>
					</tr>
					<tr>
						<td>{{ Form::label('password', 'Password') }}</td>
						<td>{{ Form::password('password',array('placeholder' => 'Password')) }}
						<br>{{ $errors->first('password') }}
						</td>
					</tr>
					<tr>
						<td>{{ Form::label('password', 'Confirm Password') }} </td>
						<td>{{ Form::password('password_confirm',array('placeholder' => 'Confirm Password'))}} 
						<br>{{ $errors->first('password_confirm') }}
						</td>
					</tr>
					<tr>
						<td colspan="2">{{ Form::submit('Sign up',array('name'=>'signup','class'=>"btn btn-info")) }}</td>
					</tr>
				</table>
			{{ Form::close() }}
			</td>
		</tr>
	</table>
	@else
	 <a href="logout">Logout</a>
	@endif
@stop