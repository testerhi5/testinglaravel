@extends('layouts.default')
@section('content')
	
	@if(Session::has('message'))
		{{ Session::get('message')}}
	@endif
	
	
			{{ Form::open(array('url' => 'upform','method'=>'post','files'=>true)) }}
				<p>
					<!--@if ($errors->has())
					
						@foreach ($errors->all() as $error)
						<span class="alert-error clearfix">	{{ $error }}	</span>
						@endforeach
					
					@endif-->
				</p>
				<style type="text/css">input#pictures{display:none;}</style>
				<table  class="table" border="1" style="text-align:center;">
					<tr>
						<th colspan="2">Update Ads</th>
					</tr>
					@if (!empty($ad))
		<tr>
						<td>{{ Form::hidden('id',$ad->id) }}</td>
					</tr>
					
					<tr>
						<td>{{ Form::label('picture', 'Image') }}</td>
						<td>{{ Form::label('pictures','Choose File')}}</td>
						{{ Form::file('picture',array('id'=>'pictures')) }}
						
						<td><img src="<?php echo URL::to('/uploads/thumb/'.$ad->picture)?>"/></td>
						<br>{{ $errors->first('picture') }}
						</td>
					</tr>
					
					
					<tr>
					<td>{{ Form::label('surface', 'Surface') }}</td>
						<td>{{ Form::text('surface',$ad->surface, Input::old('surface'), array('placeholder' => 'Surface')) }} 
						<br>{{ $errors->first('surface') }}
						</td>
					</tr>
					
					<tr>
					<td>{{ Form::label('price', 'Price') }}</td>
						<td>{{ Form::text('price',$ad->price, Input::old('price'), array('placeholder' => 'Price')) }} 
						<br>{{ $errors->first('price') }}
						</td>
					</tr>
					
					
					<tr>
					<td>{{ Form::label('address', 'Address') }}</td>
					
						<td>{{ Form::text('address',$ad->address, Input::old('address'), array('placeholder' => 'Address')) }} 
						<br>{{ $errors->first('address') }}
						</td>
					</tr>
					
					<tr>
					<td>{{ Form::label('description', 'Description') }}</td>
					
						<td>{{ Form::text('description',$ad->description,Input::old('description'), array('placeholder' => 'Description')) }} 
						<br>{{ $errors->first('description') }}
						</td>
					</tr>
					

						<tr>
						<td>{{ Form::label('type', 'Type') }}</td>
						<td>{{ 'Rent' }} {{Form::radio('type', 'rent', ($ad->type=='rent') ? true : false)}} 
						{{ 'Buy' }} {{Form::radio('type', 'buy',($ad->type=='buy') ? true : false)}}</td>
						<br>{{ $errors->first('type') }}
						
					</tr>
					
					
					<tr>
						<td>{{ Form::label('property_type', 'Property_type') }}</td>
	<td>{{ 'Appartment' }} {{Form::radio('property_type', 'appartment',($ad->property_type=='appartment') ? true : false)}} 
			{{ 'Villa' }}{{Form::radio('property_type', 'villa', ($ad->property_type=='villa') ? true : false)}}</td>
						
						<br>{{ $errors->first('property_type') }}
					
					</tr>
					
					<tr>
						<td>{{ Form::label('isactive', 'Isactive') }}</td>
						<td>{{ 'Yes' }} {{Form::radio('isactive', 'y', ($ad->isactive=='y') ? true : false)}} {{ 'No' }} {{Form::radio('isactive', 'n', ($ad->isactive=='n') ? true : false)}}</td>
						<br>{{ $errors->first('isactive') }}
						
					</tr>
					<tr><td colspan="2" style="text-align:center;">{{ Form::submit('Update Ad',array('name'=>'updatebtn','class'=>"btn btn-info")) }}</td></tr>
					
				</table>
			{{ Form::close() }}
			
	@else
	<td>'not found' </td>
	@endif
	 
@stop