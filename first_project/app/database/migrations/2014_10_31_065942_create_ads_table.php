<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('picture');
            $table->string('surface',100);
            $table->float('price');
			$table->text('address');
			$table->text('description');
			$table->enum('type', array('rent', 'buy'))->default('rent');
			$table->enum('property_type', array('appartment', 'villa'))->default('appartment');
			$table->enum('isactive', array('y', 'n'))->default('y');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads');
	}


}
