<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', 'LoginController@showLogin');
Route::post('login', 'LoginController@postLogin');
 
Route::get('login/test', 'LoginController@test'); 
Route::get('/', 'HomeController@showWelcome');
Route::get('ads/new', 'HomeController@adsform');
Route::post('form', 'HomeController@postads');
Route::resource('updateform','HomeController@updateads');
Route::post('upform', 'HomeController@postadupdate');
Route::resource('deletead','HomeController@deletead');
Route::get('accountinfo', 'HomeController@showInfo');




Route::get('logout', function() {
	Auth::logout();
    return Redirect::to('/login');
});

/*
Route::controller('login','LoginController');
*/